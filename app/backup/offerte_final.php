<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<title>Offerte</title>

<link href="css/stijl.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">

function doePrinten()
{
window.print()	
}
</script>
</head>
<body style="background-image:url('images/background_header.png');background-position:top;background-repeat:no-repeat;margin-top:0px;">
<div style="width:100%;text-align:right"><a href="javascript:doePrinten()"><img src="images/icon_print.png" border="0" width="24" title="Klik hier om deze offerte te printen" /></a></div>
<?
include ("config/config.php");
$indid = $_GET[indid];
$logged_user_id = $_GET[userid];

$querysoftcheck=mysql_query("SELECT * FROM mkc_users WHERE un_user_id = '$logged_user_id'") or die (mysql_error());
$logcheck = mysql_fetch_object($querysoftcheck);



if($logcheck->un_user_id != "")
{

$querymeta2 = mysql_query("SELECT * FROM mkc_offerte_index WHERE offerte_id = '$indid' ORDER BY id ASC") or die (mysql_error());
$metainfo = mysql_fetch_object($querymeta2);

if($metainfo->aanvrager == "")
$aanhef = "Klant";
else
$aanhef = $metainfo->aanvrager;

$datum_aangemaakt_mooi = date("d-m-Y H:i", $metainfo->datumstamp);

$querycalc = mysql_query("SELECT * FROM mkc_users WHERE un_user_id = '$metainfo->user_id' ORDER BY id ASC") or die (mysql_error());
$calculator = mysql_fetch_object($querycalc);


?>
<h2>Offerte DMD-Klussen</h2>

<table width="60%" cellpadding="0" cellspacing="10">
<tr><td valign="top" width="50%" style="border:1px solid #C0C0C0;background-color:#FFFFFF;">

<table cellpadding="0" cellspacing="5" width="100%">
<tr><td>Naam project:</td><td><?=$metainfo->naam_project?></td>
<tr><td>Datum aanvraag:</td><td><?=$metainfo->datum_aanvraag?></td></tr>
<tr><td>Datum indienen:</td><td><?=$metainfo->datum_indienen?></td></tr>
<tr><td>Calculator:</td><td><? echo"$calculator->voornaam $calculator->achternaam"; ?></td></tr>
</table>

</td>
<td valign="top" width="50%" style="border:1px solid #C0C0C0;background-color:#FFFFFF;">

<table cellpadding="0" cellspacing="5" width="100%">
<tr><td>Aanvrager:</td><td><?=$metainfo->aanvrager?></td></tr>
<tr><td>Adres:</td><td><?=$metainfo->adres?></td></tr>
<tr><td>Locatie:</td><td><?=$metainfo->locatie?></td></tr>
<tr><td>Architect:</td><td><?=$metainfo->architect?></td></tr>
<tr><td>E-mail/tel:</td><td><?=$metainfo->email?></td></tr>
</table>

</td>
</tr>
</table> 
</div>

<?


$query3 = mysql_query("SELECT * FROM mkc_offerte WHERE offerte_id = '$indid' ORDER BY categorie ASC") or die (mysql_error());
while ($result = mysql_fetch_object($query3))
	{

$uren = $result->norm_tijd/60;
$uren_rond = floor($uren);
$minuten = ($uren-$uren_rond)*60;
$minuten_rond = round($minuten);
if($minuten_rond < 10)
$minuten_rond = "0".$minuten_rond;

$line_norm_tijd = $uren_rond.":".$minuten_rond;
$line_materiaal = $result->prijs;
$line_aantal = $result->aantal;
$line_tarief = $result->loon_tarief;



//berekende waardes
$duur = $result->aantal*$result->norm_tijd;
$duur_uren = $duur/60;
$duur_uren_rond = floor($duur_uren);

$duur_minuten = ($duur_uren-$duur_uren_rond)*60;
$duur_minuten_rond = round($duur_minuten);
if($duur_minuten_rond < 10)
$duur_minuten_rond = "0".$duur_minuten_rond;

$materiaal_prijs = round(($result->prijs*$result->aantal),2);
$loon_prijs = round(($duur_uren*$line_tarief),2);
$totaal_prijs = $materiaal_prijs+$loon_prijs;

//totalen uitrekenen
$cum_loon = $loon_prijs+$cum_loon;
$cum_materiaal_prijs = $materiaal_prijs+$cum_materiaal_prijs;
$cum_totaal_prijs = $totaal_prijs+$cum_totaal_prijs;
$cum_duur = $duur+$cum_duur;

	}
	
include('offerte_eindcalculaties.php');

echo"
<table width=\"77%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" style=\"border: 1px solid #C0C0C0;background-color:#FFFFFF\">
<tr>
<td class=\"heading\" align=\"center\"><b>FINANCIEEL OVERZICHT</b></td>
</tr>
<tr>
<td>

<table width=\"100%\">
<tr><td width=\"100\">Directe kosten:</td>
<td>manuren</td><td>$cum_duur_uren_rond uur : $cum_duur_minuten_rond min. </td><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>gem. uurloon</td><td>&euro; $gem_tarief_mooi</td><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>loonkosten</td><td>&nbsp;</td><td>&euro; $cum_loon_mooi</td><td>&nbsp;</td></tr>
<tr><td colspan=\"5\">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>materiaalkosten</td><td>&euro; $cum_materiaal_prijs_mooi</td><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>werk derden</td><td>&euro; $werk_derden_mooi</td><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>totaal materiaal en werk derden</td><td>&nbsp;</td><td>&euro; $tot_mat_derden_mooi</td><td>&nbsp;</td></tr>
<tr><td colspan=\"5\">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td><b>TOTAAL DIRECTE KOSTEN</b></td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\"><b>&euro; $sum_dir_kosten_mooi</b></td></tr>

<tr><td colspan=\"5\" style=\"border-top:1px solid #000000\">&nbsp;</td></tr>

<tr><td width=\"100\">Toeslagen:</td>
<td>uurloon projectleider</td><td>&euro; $uurloon_projectleider_mooi</td><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>toeslag percentage over uren werkzh.</td><td>$per_toesl_werkzh %</td><td>&euro; $tot_toesl_werkzh_mooi</td><td>&nbsp;</td></tr>
<tr><td colspan=\"5\">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>toeslag materiaal</td><td>$per_toesl_materiaal %</td><td> &euro; $tot_toesl_materiaal_mooi</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>toeslag werk derden</td><td>$per_toesl_derden %</td><td> &euro; $tot_toesl_derden_mooi</td><td>&nbsp;</td></tr>
<tr><td colspan=\"5\">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td><b>TOTAAL TOESLAGEN</b></td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\"><b>&euro; $sum_toeslagen_mooi</b></td></tr>

<tr><td colspan=\"5\" style=\"border-top:1px solid #000000\">&nbsp;</td></tr>

<tr><td width=\"100\">Voorzieningen<br>en<br>afvoer materiaal:</td>
<td>veiligheidsplan</td><td>$per_veiligheidsplan %.</td><td>&euro; $tot_veiligheidsplan_mooi</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>werktekeningen</td><td>$per_werktekeningen %</td><td>&euro; $tot_werktekeningen_mooi</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>rivisietekeningen</td><td>$per_rivisietekeningen %</td><td>&euro; $tot_rivisietekeningen_mooi</td><td>&nbsp;</td></tr>
<tr><td colspan=\"5\">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>Afvoer materiaal/verwijderingsbijdrage</td><td>$per_verw_bijdrage %</td><td>&euro; $tot_verw_bijdrage</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>keet- en wasruimte</td><td>$per_wasruimte %</td><td> &euro; $tot_wasruimte_mooi</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>huur steigers</td><td>$per_steigers %</td><td> &euro; $tot_steigers_mooi</td><td>&nbsp;</td></tr>
<tr><td colspan=\"5\">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td><b>TOTAAL VOORZIENINGEN</b></td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\"><b>&euro; $sum_voorz_en_afvoer_mooi</b></td></tr>

<tr><td colspan=\"5\" style=\"border-top:1px solid #000000\">&nbsp;</td></tr>

<tr><td width=\"100\">Stelposten:</td>
<td>post onvoorzien</td><td>&nbsp;</td><td>&euro; $tot_onvoorzien_mooi</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>telefoonkosten</td><td>&nbsp;</td><td>&euro; $tot_telefoonkosten_mooi</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>parkeerkosten/reiskosten</td><td>&nbsp;</td><td>&euro; $tot_parkeerkosten_mooi</td><td>&nbsp;</td></tr>
<tr><td colspan=\"5\">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td><b>TOTAAL STELPOSTEN</b></td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\"><b>&euro; $sum_stelposten_mooi</b></td></tr>

<tr><td colspan=\"5\" style=\"border-top:1px solid #000000\">&nbsp;</td></tr>

<tr><td width=\"100\">Winst en Risico:</td>
<td>totaal bedrag ex. BTW</td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\">&euro; $supertot_exbtw_mooi</td></tr>
<tr><td colspan=\"5\">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td><b>winst en risico</b></td><td>$per_risico %</td><td>&nbsp;</td><td align=\"right\"><b>&euro; $tot_risico_mooi</b></td></tr>
<tr><td colspan=\"5\">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td><b>TOTALE AANNEEMSOM ex. BTW</td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\"><b>&euro; $supertot_aanneemsom_ex_mooi</b></td></tr>

<tr><td colspan=\"5\" style=\"border-top:1px solid #000000\">&nbsp;</td></tr>

<tr><td width=\"100\">&nbsp;</td>
<td>BTW $btw_loon_mooi % (loonkosten, werkderden e.a.)</td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\">&euro; $tot_btwbedrag_loon_mooi</td></tr>
<tr><td width=\"100\">&nbsp;</td>
<td>BTW $btw_overig_mooi % (overige kosten)</td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\">&euro; $tot_btwbedrag_overig_mooi</td></tr>

<tr><td colspan=\"5\">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td><b>TOTALE AANNEEMSOM inclusief BTW</td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\"><b><span style=\"font-size:20px;border-bottom:10px solid #C0C0C0\">&euro; $supertot_aanneemsom_inc_mooi</span></b></td></tr>
<tr><td colspan=\"5\">&nbsp;</td></tr>
</table>
<br>

</td>
</tr>
</table>
<br>
<br>
<br>
<br>
<div id=\"footer\" style=\"width:99%;text-align:center;border-top:1px solid #C0C0C0;font-size:10px;\">
DMD Klussen  |  A Nozemanstraat 13 1  |  1065 TP Amsterdam<br />
KvK  34214188 | TEL.06-45624747
</div>
";

}
else
echo"Je moet ingelogd zijn om deze pagina te kunnen zien.";
?>
<br /><br /><br /><br />
</body>
</html>
