<?php

//gebruiker binnen krijen
$un_user_id = $logged_user_id;

//check of er nog steeds is ingelogd

if($logged_user_id != "")
{

?>
<br />
<br />
Om de database te kunnen beheren dient eerst ingelogd te worden op de database-tool phpmyadmin via <a href="http://66.7.213.74:2082/3rdparty/phpMyAdmin/index.php" target="_blank">deze link</a> (gebruik logingegevens van C-panel).<br /><br />
<span style="color:#FF0000">Let op: Het beheren van database gegevens is een naukweurige klus. Bij verkeerd handelen kan de database per ongeluk verwijderd worden etc.</span><br />
<br />
Op de database mogen twee zaken uitgevoerd worden die hieronder staan uitgelegd:<br />
<br />
<br />
<b>Activiteiten DB onderhouden:</b><br />
<li>Klik in he linker paneel op de tabel mkc_activiteiten (<a href="images/afb1.png" target="_blank">zie screenshot</a>)<br />
<li>Via de knop 'verkennen' boven in (nr1) wordt de lijst met activiteiten getoond (<a href="images/afb3.png" target="_blank">zie screenshot</a>)<br />
<li>Via de pijltjes knoppen (nr2) kan er verder en teruggebladerd worden door de records<br />
<li>Via het potloodje icoontje (nr 3) kan een item bewerkt worden<br />
<li>Na op het icoontje geklikt te hebben kom je in het bewerkscherm (<a href="images/afb4.png" target="_blank">zie screenshot</a>)<br />
<li>Wijzig hier de gegevens, maar pas nooit het id-veld aan. Klik op de knop start om te bevestigen<br />
<li>Via het rode delete kruisje icoontje op elke regel (nr 4) kan een item geheel verwijderd worden (<a href="images/afb3.png" target="_blank">zie screenshot</a>)<br />
<li>Het invoeren van items kan eventueel via deze tool, maar werkt net zo makkelijk via de voorkant van het systeem<br /><br />
<br />
<br /><b>Gebruikers onderhouden:</b><br />
<li>Klik in he linker paneel op de tabel mkc_users (<a href="images/afb1.png" target="_blank">zie screenshot</a>)<br />
<li>Via de knop "invoegen" kan er een nieuwe gebruiker worden aangemaakt (<a href="images/afb2.png" target="_blank">zie screenshot</a><br />
<li>Vul in het scherm de benodigde gegevens in, maar laat het 'id' veld leeg. Klik op de knop 'start' om te nieuwe gebruiker in te voegen<br />
<li>Via de knop 'verkennen' bovenin het scherm wordt de lijst met bestaande gebruikers vertoond<br />
<li>Via het potloodje icoontje kan een gebruiker bewerkt worden, via het rode kruisje kan een gebruiker verwijderd worden<br />
<br />
<br />
<span style="color:#FF0000">Let op: pas nooit zonder voldoende kennis andere zaken aan in de database!</span>


<br><br><br><br><br><br><br><br><br><br>

<?
}
else
echo"Je dient eerst in te loggen om deze informatie te mogen zien";
?>
