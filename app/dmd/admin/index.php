<?php
include('../config/config.php');

$page = $_GET[page];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<title>Admin Panel - Offerte programma</title>

<link href="css/stijl.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>	
<script type="text/javascript" src="../js/randomizer.js"></script>
<script type="text/javascript" src="../js/getposition.js"></script>
<script type="text/javascript" src="../js/searchdropdown.js"></script>
<script type="text/javascript" src="../js/vulvelden.js"></script>
<script type="text/javascript" src="../js/confirm.js"></script>
<script type="text/javascript" src="../js/readmore.js"></script>
<script type="text/javascript" src="../js/jquery.DOMWindow.js"></script> 


</head>

<body style="background-image:url('images/background_header.png');background-position:top;background-repeat:no-repeat">
<h2>ZZP-Maatschap Admin Panel</h2>
<br /><br /><br />



<?php


echo"&nbsp;&nbsp;&nbsp;
<a href=\"?page=1\" class=\"mainlinks\">Alle Offertes</a>
<a href=\"?page=2\" class=\"mainlinks\">Gebruikers</a>  
<a href=\"?page=3\" class=\"mainlinks\">Norm Activiteiten</a>  
<a href=\"?page=4\" class=\"mainlinks\">Nieuwe Activiteiten</a> 
<a href=\"?page=5\" class=\"mainlinks\">Database configuratie</a>  
<a href=\"javascript:window.close()\" class=\"mainlinks\">uitloggen</a>
<div style=\"border-top:1px solid #666;width:100%;margin-left:0px;margin-right:0px;margin-top:4px;background-color:#FFFFFF\">
<br />";

if($page == 1)
include('alle_offertes.php');
else if($page == 2)
include('gebruikers.php');
else if($page == 3)
include('norm_activiteiten.php');
else if($page == 4)
include('nieuwe_activiteiten.php');
else if($page == 5)
include('uitleg_database.php');
else
echo"<br><br><br><br><br>";

?>

</div>

<script type="text/javascript"> 
$('.absoluteIframeDOMWindow').openDOMWindow({
height:700,
width:850,
positionType:'centered',
eventType:'click',
windowSource:'iframe',
windowPadding:0,
loader:1,
loaderImagePath:'animationProcessing.gif',
loaderHeight:16,
loaderWidth:17
});
</script>

</body>
</html>