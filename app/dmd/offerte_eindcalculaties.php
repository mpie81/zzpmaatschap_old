<?

//afronden en komma's geven aan while bedragen
$cum_materiaal_prijs_mooi = number_format($cum_materiaal_prijs, 2, ',', '.');
$cum_loon_mooi = number_format($cum_loon, 2, ',', '.');
$totaal_prijs_mooi = number_format($totaal_prijs, 2, ',', '.');
//einde afronden en komma;s geven aan while bedragen


//totale uren naberekenen in juist formaat
$cum_duur_uren = $cum_duur/60;
$cum_duur_uren_rond = floor($cum_duur_uren);

$cum_duur_minuten = ($cum_duur_uren-$cum_duur_uren_rond)*60;
$cum_duur_minuten_rond = round($cum_duur_minuten);
if($cum_duur_minuten_rond < 10)
$cum_duur_minuten_rond = "0".$cum_duur_minuten_rond;	

//totalen directe kosten
$werk_derden = $metainfo->werk_derden;
$werk_derden_mooi = number_format($werk_derden, 2, ',', '.');
$tot_mat_derden = $cum_materiaal_prijs+$werk_derden;
$tot_mat_derden_mooi = number_format($tot_mat_derden, 2, ',', '.');
$sum_dir_kosten = $cum_loon+$tot_mat_derden;
$sum_dir_kosten_mooi = number_format($sum_dir_kosten, 2, ',', '.');
//einde totalen directe kosten


//toeslagen
//toeslag werkzh
$uurloon_projectleider = $metainfo->uurloon_projectleider;
$uurloon_projectleider_mooi = number_format($uurloon_projectleider, 2, ',', '.');
$per_toesl_werkzh = $metainfo->toeslag_werkzh;
$tot_toesl_werkzh = $cum_duur_uren*($per_toesl_werkzh/100)*$uurloon_projectleider;
$tot_toesl_werkzh = round($tot_toesl_werkzh,2);
$tot_toesl_werkzh_mooi = number_format($tot_toesl_werkzh, 2, ',', '.');

//toeslag materiaal
$per_toesl_materiaal = $metainfo->toeslag_materiaal;
$tot_toesl_materiaal = $cum_materiaal_prijs*($per_toesl_materiaal/100);
$tot_toesl_materiaal = round($tot_toesl_materiaal,2);
$tot_toesl_materiaal_mooi = number_format($tot_toesl_materiaal, 2, ',', '.');
//toeslag derden
$per_toesl_derden = $metainfo->toeslag_derden;
$tot_toesl_derden = $werk_derden*($per_toesl_derden/100);
$tot_toesl_derden = round($tot_toesl_derden,2);
$tot_toesl_derden_mooi = number_format($tot_toesl_derden, 2, ',', '.');

$sum_toeslagen = $tot_toesl_werkzh+$tot_toesl_materiaal+$tot_toesl_derden;
$sum_toeslagen_mooi = number_format($sum_toeslagen, 2, ',', '.');
//einde toeslagen

//voorz en afvoer
$per_veiligheidsplan = $metainfo->veiligheidsplan;
$tot_veiligheidsplan = $sum_dir_kosten*($per_veiligheidsplan/100);
$tot_veiligheidsplan = round($tot_veiligheidsplan,2);
$tot_veiligheidsplan_mooi = number_format($tot_veiligheidsplan, 2, ',', '.');
$per_werktekeningen = $metainfo->werktekeningen;
$tot_werktekeningen = $sum_dir_kosten*($per_werktekeningen/100);
$tot_werktekeningen = round($tot_werktekeningen,2);
$tot_werktekeningen_mooi = number_format($tot_werktekeningen, 2, ',', '.');
$per_rivisietekeningen = $metainfo->rivisietekeningen;
$tot_rivisietekeningen = $sum_dir_kosten*($per_rivisietekeningen/100);
$tot_rivisietekeningen = round($tot_rivisietekeningen,2);
$tot_rivisietekeningen_mooi = number_format($tot_rivisietekeningen, 2, ',', '.');
$per_verw_bijdrage = $metainfo->verw_bijdrage;
$tot_verw_bijdrage = $sum_dir_kosten*($per_verw_bijdrage/100);
$tot_verw_bijdrage = round($tot_verw_bijdrage,2);
$tot_verw_bijdrage_mooi = number_format($tot_verw_bijdrage, 2, ',', '.');
$per_wasruimte = $metainfo->wasruimte;
$tot_wasruimte = $sum_dir_kosten*($per_wasruimte/100);
$tot_wasruimte = round($tot_wasruimte,2);
$tot_wasruimte_mooi = number_format($tot_wasruimte, 2, ',', '.');
$per_steigers = $metainfo->steigers;
$tot_steigers = $sum_dir_kosten*($per_steigers/100);
$tot_steigers = round($tot_steigers,2);
$tot_steigers_mooi = number_format($tot_steigers, 2, ',', '.');

$sum_voorz_en_afvoer = $tot_veiligheidsplan+$tot_werktekeningen+$tot_rivisietekeningen+$tot_verw_bijdrage+$tot_wasruimte+$tot_steigers;
$sum_voorz_en_afvoer_mooi = number_format($sum_voorz_en_afvoer, 2, ',', '.');
//einde voorz en afvoer

//stelposten
$tot_onvoorzien = $metainfo->onvoorzien;
$tot_onvoorzien = round($tot_onvoorzien,2);
$tot_onvoorzien_mooi = number_format($tot_onvoorzien, 2, ',', '.');
$tot_telefoonkosten = $metainfo->telefoonkosten;
$tot_telefoonkosten = round($tot_telefoonkosten,2);
$tot_telefoonkosten_mooi = number_format($tot_telefoonkosten, 2, ',', '.');
$tot_parkeerkosten = $metainfo->parkeerkosten;
$tot_parkeerkosten = round($tot_parkeerkosten,2);
$tot_parkeerkosten_mooi = number_format($tot_parkeerkosten, 2, ',', '.');
$sum_stelposten = $tot_onvoorzien+$tot_telefoonkosten+$tot_parkeerkosten;
$sum_stelposten_mooi = number_format($sum_stelposten, 2, ',', '.');
//einde stelposten

$supertot_exbtw = $sum_dir_kosten+$sum_toeslagen+$sum_voorz_en_afvoer+$sum_stelposten;
$supertot_exbtw_mooi = number_format($supertot_exbtw, 2, ',', '.');
$per_risico = $metainfo->risico;
$tot_risico = $supertot_exbtw*($per_risico/100);
$tot_risico = round($tot_risico,2);
$tot_risico_mooi = number_format($tot_risico, 2, ',', '.');

//eindtotalen
$supertot_aanneemsom_ex = $supertot_exbtw+$tot_risico;
$supertot_aanneemsom_ex_mooi = number_format($supertot_aanneemsom_ex, 2, ',', '.');


//BTW bedragen
$basis_btwloon = $cum_loon+$werk_derden;
$btw_loon_mooi = $metainfo->btw_loon;
$btw_loon = $btw_loon_mooi/100;
$tot_btwbedrag_loon = $btw_loon*$basis_btwloon;
$tot_btwbedrag_loon = round($tot_btwbedrag_loon,2);
$tot_btwbedrag_loon_mooi = number_format($tot_btwbedrag_loon, 2, ',', '.');

//optionele btw op parkeerkosten wordt verwerkt in het btw-hoog bedrag
$parkeer_btw_status = $metainfo->parkeer_btw;
if($parkeer_btw_status == "ja")
$basis_btwoverig = $supertot_aanneemsom_ex-$cum_loon-$werk_derden;
else
$basis_btwoverig = $supertot_aanneemsom_ex-$cum_loon-$werk_derden-$tot_parkeerkosten;

$btw_overig_mooi = $metainfo->btw_overig;
$btw_overig = $btw_overig_mooi/100;
$tot_btwbedrag_overig = $btw_overig*$basis_btwoverig;
$tot_btwbedrag_overig = round($tot_btwbedrag_overig,2);
$tot_btwbedrag_overig_mooi = number_format($tot_btwbedrag_overig, 2, ',', '.');
//einde BTW bedragen

$supertot_aanneemsom_inc = $supertot_aanneemsom_ex+$tot_btwbedrag_overig+$tot_btwbedrag_loon;
$supertot_aanneemsom_inc_mooi = number_format($supertot_aanneemsom_inc, 2, ',', '.');
//einde eindtotalen

//gem uurtarief (nieuw)
//gebruik @ omdat wanneer leeg delen door nul flauwekul is
$gem_tarief = @($cum_loon/$cum_duur)*60;
$gem_tarief_mooi = number_format($gem_tarief, 2, ',', '.');
//einde gem uurtarief



?>